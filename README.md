# multi_chunk #

Copyright 2021 TU Graz

### Author ###

Michael Kerber

### Summary ###

This software computes the multi-chunk algorithm based on the paper

Ulderico Fugacci, Michael Kerber: Chunk Reduction for Multi-Parameter Persistent Homology. Proceedings of the 35th International Symposium on Computational Geometry (SoCG 2019), pp.37:1-37:14. Available at [arXiv:1812.08580](http://arxiv.org/abs/1812.08580)

The software accepts a text file in scc2020 format (explained [here](https://bitbucket.org/mkerber/chain_complex_format))
and computes a (usually much smaller) text file that is also in scc2020 format and represents a chain complex quasi-isomorphic to the input one.

### Requirements ###

The library is written in C++ and requires a compiler that realizes the C++14-standard. Also *cmake* is required in version 3.9 or higher.

Further dependencies are optional:

* multi-chunk uses the *phat* library for representing matrices, but it comes with its own (slightly modified) version of *phat*. If desired, the phat version can be changed by setting the PHAT_INCLUDE_DIR variable of cmake to the appropriate include-path of phat. At least version 1.6 of *phat* is required.
* The timer library of *Boost* is used to measure the performance of the substeps of the algorithm. If *Boost* is not found, the timer are simply disabled. The code was tested using *Boost* 1.71
* *OpenMP* is used to parallelize the execution (see below)

## Usage

A CMakeLists.txt file is included. The executables are created using

	cmake .
	make

The programs multi_chunk and multi_chunk_sequential require both an input file and an output file. The former is only compiled if *OpenMP* is installed, and uses by default the maximal number of cores in the computation, while multi_chunk_sequential does not use *OpenMP*. For both executables, the -v option shows the progress of the computation, and timing information if *Boost* is enabled. 


### Interface ###

To use the implementation within other projects, use the function given in the file include/multi_chunk.h:


    template< typename ColumnType=phat::vector_vector,
              typename CoordinateTraits=mpp_utils::Coordinate_traits_with_map<double>
    >
    void compress(const std::string& infile,
		  const std::string& outfile)

It takes two strings representing the input and the output file. The template arguments have default values, but can be modified to adapt to other contexts. Their meanings are:

* ColumnType: The data structure to store the columns of matrices. All column types of the phat library are supported.
* CoordinateTraits: Defines how grade coordinates are handled. Specifically, the template parameter class should contain a type `Coordinate` which is used to store coordinates, functions `from_string` and `to_string` to convert coordinates from and to string, and a functor class `Compare` to check which of two coordinates is smaller. The default choice `Coordinate_traits_with_map<double>` interprets theinput coordinates as doubles, and writes precisely the same input string also into the output file. It is required that two distinct strings are converted into distinct double coordinates.


### Contact ###

Michael Kerber (kerber@tugraz.at)
