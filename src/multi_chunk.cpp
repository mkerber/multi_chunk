#include<iostream>
#include<string>

#include<scc/Scc.h>
#include<multi_chunk/multi_chunk.h>




int main(int argc, char** argv) {
  
  scc::verbose=false;
  mpp_utils::verbose=true;
  multi_chunk::verbose=true;

  std::string infile="";
  std::string outfile="";
  
  for(int i=1;i<argc;i++) {
    if(std::string(argv[i])=="-v") {
      multi_chunk::verbose=true;
      continue;
    }
    if(infile=="") {
      infile=argv[i];
      continue;
    }
    if(outfile=="") {
      outfile=argv[i];
      continue;
    }
    std::cout << "Ignored argument: " << argv[i] << std::endl;
  }

  if(outfile=="") {
    std::cerr << "Input file and output file needed" << std::endl;
    std::exit(1);
  }

  multi_chunk::compress(infile,outfile);

  return 0;
}
